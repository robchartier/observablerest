﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using ObservableRest;
using Xunit;

namespace UnitTests
{
    public class Tests
    {

        private int x = 0;
        private int max = 10;
        private bool hasError = false;
        [Fact]
        public async Task PollLocal()
        {

            bool exit = false;
            var observableProvider = new PollingObservableProvider();

            var subscription = observableProvider.Poll<string, int>(StringObservableCall, () =>
            {
                return x;
            },
                TimeSpan.FromMilliseconds(10),
                ThreadPoolScheduler.Instance)
                .TimeInterval()
                .Subscribe(StringConsoleReporter);


            while (x<max || hasError)
            {
                await Task.Delay(1);
            }
            Assert.True(x>=max);
            subscription.Dispose();
        }

        [Fact]
        public async Task PollRemote()
        {
            
            bool exit = false;
            var observableProvider = new PollingObservableProvider();

            var subscription = observableProvider.Poll<SensorReading, int>(DownloadSensorDataAsync,() =>
            {
                return x;
            },
                TimeSpan.FromSeconds(5),
                ThreadPoolScheduler.Instance)
                .TimeInterval()
                .Subscribe(SensorReadingConsoleReporter);


            while (x < max || hasError)
            {
                await Task.Delay(1);
            }
            Assert.True(x >= max);
            subscription.Dispose();
        }


        // A asynchronous function representing the REST call
        public IObservable<string> StringObservableCall(int y)
        {
            x = x + 1;
            return (x > max)            
                ? Observable.Throw<string>(new Exception())
                : Observable.Return(x + "-ret");
        }

        HttpClient http = new HttpClient();
        string sensorUrl = "https://thingspeak.com/channels/9/field/2.json?offset=0&round=2&average=30days=";

        public IObservable<SensorReading> DownloadSensorDataAsync(int count)
        {
            x = x + 1;

            var value = http.GetStringAsync(sensorUrl + x).Result;


            var reading = Newtonsoft.Json.JsonConvert.DeserializeObject<SensorReading>(value);


            return (x > max)
                ? Observable.Throw<SensorReading>(new Exception())
                : Observable.Return(reading);
        }

        public void StringConsoleReporter(TimeInterval<Either<Exception, string>> interval)
        {
            Trace.WriteLine("Interval: " + interval.Interval);
            var result = interval.Value;
            if (result.IsRight)
            {
                Trace.WriteLine(" Success: " + result.Right);
            }
            else
            {
                x--;
                hasError = true;
                Trace.WriteLine(" Error: " + result.Left.Message);
            }
        }

        public void SensorReadingConsoleReporter(TimeInterval<Either<Exception, SensorReading>> interval)
        {
            Trace.WriteLine("Interval: " + interval.Interval);
            var result = interval.Value;
            if (result.IsRight)
            {
                var last = (from f in result.Right?.feeds orderby f.created_at select f).FirstOrDefault();
                if (last != null)
                {
                    Trace.WriteLine(" Success: " + last.field2);
                }
                else
                {
                    Trace.WriteLine(" No readings");

                }
            }
            else
            {
                x--;
                hasError = true;
                Trace.WriteLine(" Error: " + result.Left.Message);
            }
        }


    }
}
